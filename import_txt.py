# -*- coding: utf-8 -*-
from os import path, walk, sep, putenv
from sqlite3 import dbapi2 as db

ext = ".txt"
look = "/home/kylefelipe/Documentos"
files_copy = []

"""
Cria um banco spatialite dentro da pasta colocada em 'look'
dentro desse banco, cria uma camada chamada 'aux_dados',
essa tabela recebe o nome da tabela, e o endereço completo do arquivo
a ser importado.
"""

if path.isdir(look):
    print "Creating Database..."
    db_name =  path.join(look,"dados_importados.sqlite")
    putenv("SPATIALITE_SECURITY", "relaxed")
    conn = db.connect(db_name)
    conn.enable_load_extension(True)
    conn.execute("SELECT load_extension('mod_spatialite')")
    # Init Spatial Metadata
    conn.execute('SELECT InitSpatialMetadata(1)')
    conn.commit()
    cur = conn.cursor()

    """Essa tabela irá receber o caminho do arquivo e a tabela de destino dele"""
    aux_dados = """CREATE TABLE IF NOT EXISTS "aux_dados" ("id" INTEGER NOT NULL
        PRIMARY KEY AUTOINCREMENT,
        "path" TEXT NOT NULL,
        "tab_destino" TEXT NOT NULL);"""
    cur.execute(aux_dados)
    conn.commit()

    """
    Procurando os arquivos a serem importados que estão dentro das pasta
    indicada em 'look' e gerando uma lista dos arquivos com o endereço completo
    desses arquivos.
    """
    if path.isdir(look):
        print("Caminho a procurar OK!, procurando....")
        for root, dirs, files in walk(look):
            for file in files:
                if file.lower().endswith(ext):
                    caminho = path.join(root, file)
                    if path.isfile(caminho):
                        files_copy.append(caminho)

    print("Foram encontrados {} arquivos .txt".format(len(files_copy)))

    """
    Inserindo o endereço completo do arquivo e a tabela de destino dele no banco.
    O nome da tabela de destino é formada por "arq_<indice_arquivo_na_tabela>"
    """

    data_import = []
    for i in files_copy:
        file = str(i.split(sep)[-1])
        file = str(file.split(".")[0])
        tab_destino = "arq_"+str(files_copy.index(i)+1)
        cur.execute("""INSERT INTO "aux_dados" ("path", "tab_destino")
        VALUES ("{path}", "{tabela}");""".format(path=i, tabela=tab_destino))
        conn.commit()
        data_import.append([i, tab_destino])
    """
    Inserido os dados dos arquivos nas respectivas tabelas
    """
    for i in data_import:
        file_path, tab_destino = i
        fields = []
        with open(file_path, "r") as f:
            d = f.readlines()
            head = d[0].lower().strip().split(";")
            for j in head:
                fields.append("\"{}\" TEXT".format(j))
            cria_tab = """CREATE TABLE {tabela} ("id" INTEGER NOT NULL
                PRIMARY KEY AUTOINCREMENT, {campos});""".format(tabela=tab_destino,
                    campos=", ".join(fields))
            cur.execute(cria_tab)
            conn.commit()
            fields = []
            for j in head:
                fields.append("\"{}\"".format(j))
            dados = []
            print("Inserindo dados do arquivo {}".format(file_path))
            for line in d[1:]:
                col = [str(k) for k in line.strip().split(";")]
                popula = """INSERT INTO "{tabela}" ({campos})\n VALUES({val});""".format(tabela=tab_destino,
                    campos=", ".join(fields), val=", ".join(["'{}'".format(i) for i in col]))
                cur.execute(popula)
                conn.commit()
            print("Concluido!")
    conn.close()
    print("{} arquivos importados com sucesso.".format(len(files_copy)))
else:
    print("O caminho informado não é válido!")
